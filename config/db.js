const mongoose = require('mongoose')
require('dotenv').config({path: 'variables.env'})

const connectDB = async () => {
    try{
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })

        console.log('MongoDB connected')
    }catch(e){
        console.log(e)
        process.exit(1)
    }
}

module.exports = connectDB
