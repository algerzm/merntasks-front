//Rutas para crear usuarios
const express = require('express')
const router = express.Router()
const usersController = require('../controllers/usersController')

//Crea un usuario
//api/users
router.post('/',
    usersController.createUser
)

module.exports = router
