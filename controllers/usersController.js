const User = require('../models/User')
const bcryptjs = require('bcryptjs')

exports.createUser = async (req, res) => {

    //Extraer email y password
    const {email, password} = req.body

    try{
        //Validar que el email del usuario sea unico
        let user = await User.findOne({email})

        if(user){
            return res.status(400).json({msg:'Email already exists'})
        }

        //Crear instancia de usuario
        user = new User(req.body)

        //Hashear el password
        const salt = await bcryptjs.genSalt(10)
        user.password = await bcryptjs.hash(password, salt)

        await user.save()

        res.json({msg:'User Created Succesfully!'})
    }catch(e){
        console.log(e)
        res.status(400).json({msg:'Theres an error'})
    }
}