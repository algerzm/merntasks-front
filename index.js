const express = require('express')
const connectDB = require('./config/db')

//Creear el servidor
const app = express()

//Conectar a la base de datos
connectDB()

//Habilitar express.json
app.use(express.json({extended: true}))

//Puerto de la app
const PORT = process.env.PORT || 4000

//Importar rutas
app.use('/api/users', require('./routes/users'))

//Arrancar el servidor
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})